﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;


namespace Practice_08_01_2020
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string DEFAULT_IP = "127.0.0.1";
        private int DEFAULT_PORT = 3231;

        private UdpClient listener;

        public MainWindow()
        {
            InitializeComponent();
            Listen();
        }
        private void CallOccurs(bool data)
        {
            callEndButton.IsEnabled = data;
            callStartButton.IsEnabled = !data;
            nicknameBox.IsEnabled = !data;
            ipBox.IsEnabled = !data;
            portBox.IsEnabled = !data;
        }

        private async void Listen(/*IPEndPoint sender = null*/)
        {
            /*if (sender == null)
            {
                sender = new IPEndPoint(IPAddress.Parse(DEFAULT_IP), DEFAULT_PORT);
            }*/
            IPEndPoint sender = null;

            using (listener = new UdpClient(DEFAULT_PORT))
            {
                while (true)
                {
                    var receivedData = await listener.ReceiveAsync();
                    MessageBox.Show(Encoding.UTF8.GetString(receivedData.Buffer));
                }
            }
        }
        private async Task Send(string data)
        {
            using (var udpSender = new UdpClient(DEFAULT_PORT))
            {
                var bytes = Encoding.UTF8.GetBytes(data);
                var sendedBytes = await udpSender.SendAsync(bytes, bytes.Length);
            }
        }

        private void CallEndButtonClick(object sender, RoutedEventArgs e)
        {
            CallOccurs(false);
        }

        private void CallStartButtonClick(object sender, RoutedEventArgs e)
        {
            //CallOccurs(true);
            Send(nicknameBox.Text).;
        }
    }
}
